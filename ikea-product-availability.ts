export interface IkeaProductAvailability {
  productId: string;
  price: number;
  availableStock: number;
  restockDate?: string;
  forecast?: IkeaProductAvailabilityForecast;
  findItList?: IkeaProductAvailabilityFindIt[];
}

export interface IkeaProductAvailabilityForecast {
  [key: number]: number;
}

export interface IkeaProductAvailabilityFindIt {
  partNumber?: string;
  quantity?: number;
  type?: string;
  box?: string;
  shelf?: string;
}