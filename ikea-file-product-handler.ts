import { IkeaProductHandler } from "./ikea-product-handler";
import { IkeaProduct } from "./ikea-product";
import * as csvWriter from "csv-write-stream";
import { createWriteStream } from "fs";
import * as iconv from "iconv-lite";
import * as moment from "moment";
import * as aw from "awaitify-stream";
import { basename } from "path";
import { Archiver } from "../core/utils/archiver";
import { AsyncWriteStream } from "../core/utils/async-write-stream";
import { EmailSender } from "../core/email/email-sender";

export class IkeaFileProductHandler implements IkeaProductHandler {
  private writer: AsyncWriteStream;
  private currentDate: string;
  private outFile: string;

  constructor(private mailer?: EmailSender) {}

  async initialize(): Promise<void> {
    this.currentDate = moment().format("DD.MM.YYYY");
    this.outFile = `./output/${this.currentDate}Farpost1.csv`;
    this.writer = aw.addAsyncFunctions(
      csvWriter({
        newline: "\r\n",
        headers: [
          "art",
          "cat1",
          "type2",
          "imd2D",
          "sityE",
          "NalichieF",
          "NewG",
          "priceH",
          "TextI",
          "AdressJ",
          "DostavkaK",
          "CashL",
          "GaranteM",
          "date1"
        ],
        separator: "\t"
      })
    );
    this.writer
      .pipe(iconv.encodeStream("win1251"))
      .pipe(createWriteStream(this.outFile));
  }

  async handle(product: IkeaProduct): Promise<void> {
    const name = product.productName;
    const images = product.imageList.join(',');
    return this.writer.writeAsync([
      product.productId.replace(/^S/g,"").replace(/^0+/g, ""),
      product.breadCrumbs,
        `${product.type} ${name.trimLeft()}  из IKEA от ПРИМ ИКЕА`,
        images,
        "Владивосток",
        "Под заказ",
        "Новый",
        product.price,
        [
          `${name.trim()}, ${product.type}${product.validDesign}\\n `,
          `Артикульный номер: ${product.number}\\n `,
          ` ВНИМАНИЕ!!! ДАННЫЙ ТОВАР ДОСТАВЛЯЕТСЯ ПОД ЗАКАЗ \\n ЦЕНА УКАЗАНА БЕЗ СТОИМОСТИ ДОСТАВКИ ВО ВЛАДИВОСТОК \\n\\n КАК СДЕЛАТЬ ЗАКАЗ: http://vladivostok.farpost.ru/mebel-ikea-ikea-garderob-kuhnja-shkaf-stol-stul-kreslo-divan-krovat-32621979.html \\n\\nРазмеры товара: \\n${product.metric}\\n`,
          product.benefit.length > 0 ? `Главные черты: \\n${product.benefit}` : "",
          `Инструкция по уходу: \\n${product.careInst}\\n`,
          `Описание и размеры товара: \\n${product.materials}\\n`,
          `Полезная информация: \\n${product.goodToKnow}`,
          " ПРИМИКЕА\\n \\n Наш проект - это удобный сервис покупки и доставки во Владивосток товаров известного европейского производителя мебели - IKEA International Group.\\n Наши услуги обойдутся Вам дешевле, чем у других компаний, расчет цены наиболее прозрачен.\\n Наше правило работы – честность, порядочность и гарантия качества предоставляемых услуг.\\n \\n Отзывы и более подробно о нас на VL.RU http://www.vl.ru/primikea?lsearch=%D0%BF%D1%80%D0%B8%D0%BC%D0%B8%D0%BA%D0%B5%D0%B0#comments\\n \\n СТОИМОСТЬ УСЛУГ\\n \\nСтоимость наших услуг по доставке составляет:\\n при заказе товаров на общую сумму:\\n - от 1 до 3 500 руб. - 700 руб.\\n - от 3 500 до 50 000 руб. - 20% (диваны, кресла, матрасы, зеркала, стекла, фарфор - 30%)\\n - свыше 50 000 руб. - 18% (диваны, кресла, матрасы, зеркала, стекла, фарфор - 28%)\\n \\n  ВНИМАНИЕ! Некоторые товары с фиксированной стоимостью доставки\\n \\n Оплата производится предоплатой 50% наличными в рублях или на карту Сбербанка, а окончательный расчет - после получения товара.\\n По завершении доставки вместе с товаром мы представим Вам отчетные документы по предоставленной услуге и кассовые чеки магазина IKEA, являющиеся обязательными при гарантийных случаях.\\n \\n Средний срок доставки: 2,5-3 недели с момента приобретения заказа в магазине ИКЕА Новосибирск.\\n \\n На весь товар распространяется акция бесплатной доставки по г. Владивостоку.\\n \\n Сделав заказ сейчас, вы можете существенно сэкономить.\\n \\n КАК СДЕЛАТЬ ЗАКАЗ: http://vladivostok.farpost.ru/mebel-ikea-ikea-garderob-kuhnja-shkaf-stol-stul-kreslo-divan-krovat-32621979.html\\n \\n Все наши предложения: http://www.farpost.ru/user/PRIMIKEA/  \\n\\nБЛАГОТВОРИТЕЛЬНАЯ ДЕЯТЕЛЬНОСТЬ \\n\\nНаш проект является меценатом детского дополнительного образования - Студии робототехники ROBODO (Снеговая падь) https://www.farpost.ru/vladivostok/service/children/studija-robototehniki-robodo-v-snegovoj-padi-gruppy-8-14-let-61727493.html"
        ]
          .filter(value => value.length > 0)
          .join("\\n"),
        "",
        0,
        "Сбербанк, наличные",
        "Условия гарантии на сайте производителя",
        this.currentDate
    ]);
  }
  async finalize(): Promise<void> {
    await this.writer.endAsync();
    if (this.mailer) {
      const archive = await Archiver.zip(this.outFile);
      await this.mailer.send({
        subject: `Ikea price ${this.currentDate}`,
        text: `Ikea price ${this.currentDate}`,
        attachments: [{ content: archive, filename: basename(this.outFile) + ".zip" }],
      });
    }
    return Promise.resolve();
  }
}
