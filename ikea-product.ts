import { IkeaProductAvailability } from "./ikea-product-availability";

export interface IkeaProduct {
  productId: string;
  productName: string;
  type: string;
  number: string;
  breadCrumbs: string;
  imageList: string[];
  price: string;
  familyPrice: string;
  metric: string;
  benefit: string;
  careInst: string;
  materials: string;
  goodToKnow: string;
  validDesign: string;
  productUrl: string;
  availabilityUrl: string;
  availability?: IkeaProductAvailability;
  pakageInfoList: IkeaProductPackageInfo[];
}

export interface IkeaProductPackageInfo {
  articleNumber: string;
  idx: number;
  consumerPackNo: number;
  quantity: number;
  height: number;
  length: number;
  width: number;
  diameter: number;
  weight: number;
}
