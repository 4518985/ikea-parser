import { IkeaPageVisitor } from "./ikea-page-visitor";
import { IkeaProductAvailability } from "./ikea-product-availability";
import * as cheerio from "cheerio";

export class IkeaProductAvailabilityVisitor implements IkeaPageVisitor {
  private novosibirskStoreCode = 338;

  supports(url: string): boolean {
    return url.includes("/ru/ru/iows/catalog/availability/");
  }

  visitPage({ content, handleProductAvailability, context }): void {
    if (context.product) {
      const $ = cheerio.load(content, { xmlMode: true });
      const availability = this.extractProductAvailability($, context.product.price);
      handleProductAvailability(availability);
    }
  }

  extractProductAvailability($: CheerioStatic, price: number): IkeaProductAvailability {
    const availability = $(`availability > localStore[buCode=${this.novosibirskStoreCode}]  > stock`).first();
    const productId = $(availability).find("partNumber").first().text();
    const availableStock = Number($(availability).find("availableStock").first().text()) || 0;
    const restockDate = $(availability).find("restockDate").first().text();
    const forecast = {};
    $(availability).find("forecasts > forcast").each((i, forcast) => {
      const dayOffset = Number($(forcast).find("dayOffset").first().text());
      const availableStock = Number($(forcast).find("availableStock").first().text()) || 0;
      forecast[dayOffset] = availableStock;
    });
    const findItList = [];
    $(availability).find("findItList > findIt").each((i, findIt) => {
      const partNumber = $(findIt).find("partNumber").first().text();
      const quantity = Number($(findIt).find("quantity").first().text()) || null;
      const type = $(findIt).find("type").first().text();
      const box = $(findIt).find("box").first().text();
      const shelf = $(findIt).find("shelf").first().text();
      findItList.push({ partNumber, quantity, type, box, shelf });
    });
    return { productId, price, availableStock, restockDate, forecast, findItList };
  }
}
