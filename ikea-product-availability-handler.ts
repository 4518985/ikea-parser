import { IkeaProductAvailability } from "./ikea-product-availability";

export interface IkeaProductAvailabilityHandler {
  initialize(): Promise<void>;
  handle(availability: IkeaProductAvailability): Promise<void>;
  finalize(): Promise<void>;
}