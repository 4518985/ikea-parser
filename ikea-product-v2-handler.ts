import { IkeaProductV2 } from "./ikea-product-v2";

export interface IkeaProductV2Handler {
  initialize(): Promise<void>;
  handle(product: IkeaProductV2): Promise<void>;
  finalize(): Promise<void>;
}