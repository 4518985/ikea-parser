import { IkeaPageVisitor } from "./ikea-page-visitor";
import { IkeaProductAvailability } from "./ikea-product-availability";
import * as cheerio from "cheerio";

//получение остальных товаров.
export class IkeaProductListPageVisitor_All implements IkeaPageVisitor {

  supports(url: string): boolean {
    return url.includes("/ru/ru/product-list-page/more-products");
  }

  visitPage({ content, enqueueUrl }) {
    const $ = cheerio.load(content, { xmlMode: true });
    //console.log(content);
    let cont=JSON.parse(content)['moreProducts'];
    let arr=[];
    for(let item of cont['productWindow'])
    {
        arr.push(item['pipUrl']);
        enqueueUrl(item['pipUrl']);
    }

    //enqueueUrl(arr[0]);
   // console.log('Длина массива'+arr.length);
 }
}

