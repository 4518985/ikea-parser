import { IkeaCategories } from "./ikea-categories";

export interface IkeaCategoriesHandler {
  initialize(): Promise<void>;
  handle(product: IkeaCategories): Promise<void>;
  finalize(): Promise<void>;
}