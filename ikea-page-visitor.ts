import { IkeaCategories } from './ikea-categories';
import { IkeaProductAvailability } from "./ikea-product-availability";
import { IkeaProductV2 } from "./ikea-product-v2";
import { Logger } from '../core/logger/logger';
import { DB } from "../core/database/database-connection";


interface VisitPageContext {
  product?: IkeaProductV2;
}

interface VisitPageParameters {
  url: string;
  content: string;
  enqueueUrl: (url: string, context: VisitPageContext) => void;
  handleProduct: (product: IkeaProductV2) => void;
  handleProductAvailability: (productAvailability: IkeaProductAvailability) => void;
  handleCategory: (cat: IkeaCategories) => void;
  db: DB;
  logger:Logger;
  context: VisitPageContext;
}

export interface IkeaPageVisitor {
  supports(url: string): boolean;
  visitPage(parameters: VisitPageParameters): void;
}
