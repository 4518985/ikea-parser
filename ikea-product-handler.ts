import { IkeaProduct } from "./ikea-product";

export interface IkeaProductHandler {
  initialize(): Promise<void>;
  handle(product: IkeaProduct): Promise<void>;
  finalize(): Promise<void>;
}