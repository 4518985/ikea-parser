import { IkeaPageVisitor } from "./ikea-page-visitor";
import { IkeaProduct } from "./ikea-product";
import * as cheerio from "cheerio";
import { string } from "@oclif/command/lib/flags";


export class IkeaProductListPageVisitor implements IkeaPageVisitor {
  supports(url: string): boolean {
    if(url.includes("/ru/ru/cat/tovary-products/")) return false;
    return url.includes("/ru/ru/cat/");
  }

  visitPage({ content, enqueueUrl,handleCategory, db,logger }): void {
 
    const $ = cheerio.load(content, { xmlMode: true});
    // товары
    /*
    парсинг товаров отсюда убираю. Дальше формируются ссылки и они будут считываться.
    let arr=$("div.range-revamp-product-compact__bottom-wrapper a")
      .toArray()
      .map(element => $(element).attr("href"))
      .filter(url => !url.startsWith("javascript:"));
      //.map(enqueueUrl);
      arr[0]="https://www.ikea.com/ru/ru/p/hemnes-karkas-krovati-belaya-morilka-lonset-s19210809/";
      enqueueUrl(arr[0]);
      //enqueueUrl(arr[1]);
       */
      
    //парсинг подкатегорий. 
     let arr=$("div.plp-navigation-slot-wrapper a").toArray().map(element => $(element).attr("href"))
              .filter(url => !url.startsWith("javascript:")).map(enqueueUrl);
             // enqueueUrl(arr[0]);
             // enqueueUrl(arr[1]);

    ////////////////////////////////////////////////////////
        //обработка категории. Вытянуть имя, ид, урл. Количество товаров.
        //Из бредкрамба все это можно вытянуть.
        const breadCrumbList = $("div.bc-breadcrumb li.bc-breadcrumb__list-item:not(.bc-breadcrumb__list-item--active) > a").toArray()
            .map(element => $(element).text().trim())
            .filter(text => text.length > 0);
        //.slice(1,-1);
        const breadCrumbListHref = $("div.bc-breadcrumb li.bc-breadcrumb__list-item:not(.bc-breadcrumb__list-item--active) > a").toArray()
            .map(element => $(element).attr("href").trim())
            .filter(text => text.length > 0);
        // .slice(1,-1);
        //поведение этого параметра надо исследовать. Чтобы список ид соотвествовал. Текст потом разделять и разбивать. Первую пропускать, если попадает. Продукт убирать. Или уже в скрипте обработки. Подумаю. 
        const breadCrumbListID = $("div.bc-breadcrumb li.bc-breadcrumb__list-item:not(.bc-breadcrumb__list-item--active) > a").toArray()
            .map(element => $(element).attr("data-tracking-label"))
            .filter(text => text != undefined && text.length > 0);
        //  .slice(1);
        // console.log(breadCrumbList);
        // console.log(breadCrumbListHref);
        // console.log(breadCrumbListID);

        const cat_name=breadCrumbList.pop();
        const cat_url=breadCrumbListHref.pop();
        let cat_id_1=breadCrumbListID.pop();
        //console.log(cat_id_1);
        const arr1_1 = cat_id_1.split("|").map(el => el.trim());
        let cat_id = arr1_1[arr1_1.length-1];
        //console.log("Ид=" + cat_id + " Кол-во=" + cat_name + " Кол-во 2=" + cat_url);
        //parent формирую.
        const parent=arr1_1[arr1_1.length-2];
        const parent2=breadCrumbList.join('>');
        const parent3=cat_id_1;

        ////////////////////////////////////////////////
        //количество товаров. И формирование ссылок для парсинга.
        let count_product_1 = $(".plp-revamp-filter-information").text().trim();
        //console.log("sss"+count_product_1);// .plp-revamp-filter-information__total-count > span
        const arr1 = count_product_1.split(" ").map(el => el.trim());
        let count_product = arr1[0];
        if(count_product=='')count_product='0';
        //console.log("Кол-во1:"+count_product);
        //из json
        let count_product_2 = $(".js-product-list").attr("data-category");
        let count_product2='0';
        if(count_product_2!=undefined){
            count_product_2=count_product_2.trim();
            let js = JSON.parse(count_product_2);
            const id_cat = js.id;
            count_product2 = js.totalCount;
            //console.log("Ид=" + id_cat + " Кол-во=" + count_product2 + " Кол-во 2=" + count_product);

            //формирование урл для получения всех товаров.
            let url="https://sik.search.blue.cdtapps.com/ru/ru/product-list-page/more-products?category="+cat_id+"&sort=RELEVANCE&start=0&end=10000";
            enqueueUrl(url);
            // const num_in_page=100;
            // if(count_product2>24)
            // {
            //    const numpage=Math.ceil(count_product2/num_in_page);
            //    for(let i=0;i<numpage;i++)
            //    {
            //       let url="https://sik.search.blue.cdtapps.com/ru/ru/product-list-page/more-products?category="+id_cat+"&sort=RELEVANCE&start="+i*num_in_page+"&end="+(i+1)*num_in_page;
            //       enqueueUrl(url);
            //       console.log(url);
            //       break;//для отладки.
            //    } 
            //   }
        }//end count_product
        else count_product2='0';

        //сохранение в  БД.
        let record={
          id:cat_id,
          name:cat_name,
          url:cat_url,
          parent,
          parent2,
          parent3,
          count_product:count_product,
          count_product2:count_product2
        };
        //console.log(record);
        //console.error(cat_name);
        handleCategory(record);

  }


  //////////////////////////////////////
  //вставка в базу. Сделал здесь, а не через handle category. Хотя можно и так.
  // category={
  //   id,
  //   id_cat,
  //   cat_name,
  //   url,
  //   parent,
  //    parent2,
  //    parent3,
  //   count_product
  // }

  // handleCategory(category);


/*
  let records=[];
  records[0]=record;
 let str= this.insertRecords(db,logger,records);//.catch(error => logger.error(error));
 console.log(str);
  private async insertRecords(db,logger, records) {
//на промисах. Какой то глюк непонятный. 
  return db.query(
    `
      INSERT INTO products_v2
          (id, name, url, parent, parent2,parent3, count_product, count_product2
            )
      VALUES ?
      ON DUPLICATE KEY UPDATE
      id = VALUES(id),
      name = VALUES(name),
      url = VALUES(url),
      parent = VALUES(parent),
      parent2 = VALUES(parent2)
      parent3 = VALUES(parent3)
      count_product = VALUES(count_product)
      count_product2 = VALUES(count_product2)        
      `,
    [
      records.map(record => {
        return [
          record.id,
          record.name,
          record.url,
          record.parent,
          record.parent2,
          record.parent3,
          record.count_product,
          record.count_product2
        ];
      })
    ]
  );
}
*/

}