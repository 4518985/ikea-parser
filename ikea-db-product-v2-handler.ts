import { default as PQueue } from "p-queue";
import { IkeaProductV2 } from "./ikea-product-v2";
import { IkeaProductV2Handler } from "./ikea-product-v2-handler";
import { DB } from "../core/database/database-connection";
import { Logger } from "../core/logger/logger";

interface ProductV2Record {
  id: string;
  name: string;
  number: string;
  price: number;
  shortText: string;
  shortText_2: string;
  description: string;
  
  breadCrumbList: string;
  breadCrumbListHref: string;
  breadCrumbListID: string;
  imageList: string;
  
  weight: number;
  variations:string;
  environmentAndMaterial: string;
  measurement : string;
  recommendationsSCU:string;
  url: string;
}

export class IkeaDbProductV2Handler implements IkeaProductV2Handler {
  private records: ProductV2Record[] = [];
  private recordQueue = new PQueue({
    concurrency: 1
  });

  constructor(private db: DB, private logger: Logger) {}

  async initialize(): Promise<void> {}

  handle(product: IkeaProductV2): Promise<void> {
    this.recordQueue.add(() =>
      this.insertRecord({
        id: product.id,
        name: product.name,
        number: product.number,

        price: product.price,
        shortText: product.shortText,
        shortText_2: product.shortText_2,
        description: product.description,
        
        breadCrumbList : product.breadCrumbList.join("|"),
        breadCrumbListHref: product.breadCrumbListHref.join("|"),
        breadCrumbListID: product.breadCrumbListID.join("|"),
        imageList: product.imageList.join("|"),
        
        weight: product.weight,
        variations: product.variations,
        environmentAndMaterial: product.environmentAndMaterial,
        measurement : product.measurement,
        recommendationsSCU: product.recommendationsSCU,
        url: product.url,

      }).catch(error => this.logger.error(error))
      ,{priority:2}
    );
    return Promise.resolve();
  }

  async finalize(): Promise<void> {
    this.recordQueue.add(() =>
      this.insertRecords(this.records).catch(error => this.logger.error(error))
      ,{priority:2}
    );
    await this.recordQueue.onIdle();
    return Promise.resolve();
  }

  private async insertRecords(records: ProductV2Record[]) {
    if (records.length === 0) {
      return Promise.resolve();
    }

    return this.db.query(
      `
        INSERT INTO products_v2
            (id, name, number, price, shortText, shortText_2, description, breadCrumbList, breadCrumbListHref, breadCrumbListID, imageList,
              weight,
              variations,
              environmentAndMaterial,
              measurement,
              recommendationsSCU,
              url
              )
        VALUES ?
        ON DUPLICATE KEY UPDATE
        id = VALUES(id),
        name = VALUES(name),
        number = VALUES(number),
        price = VALUES(price),
        shortText = VALUES(shortText),
        shortText_2 = VALUES(shortText_2),
        description = VALUES(description),
        breadCrumbList = VALUES(breadCrumbList),
        breadCrumbListHref = VALUES(breadCrumbListHref),
        breadCrumbListID = VALUES(breadCrumbListID),
        imageList = VALUES(imageList),
        weight = VALUES(weight),
        variations = VALUES(variations),
        environmentAndMaterial = VALUES(environmentAndMaterial),
        measurement = VALUES(measurement),
        recommendationsSCU = VALUES(recommendationsSCU),
        url = VALUES(url)
        `,
      [
        records.map(record => {
          return [

            record.id,
            record.name,
            record.number,
            record.price,
            record.shortText,
            record.shortText_2,
            record.description,
            
            record.breadCrumbList,
           record. breadCrumbListHref,
           record.breadCrumbListID,
            record.imageList,
            
            record.weight,
            record.variations,
            record.environmentAndMaterial,
            record.measurement,
            record.recommendationsSCU,
            record.url

          ];
        })
      ]
    );
  }

  private async insertRecord(record: ProductV2Record) {
    let result = Promise.resolve();
    this.records.push(record);
    if (this.records.length >= 35) {
      //console.log('Скл запрос');
      result = this.insertRecords(this.records);
      this.records.length = 0;
    }

    return result;
  }
}
