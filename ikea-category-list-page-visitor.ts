import { IkeaPageVisitor } from "./ikea-page-visitor";
import * as cheerio from "cheerio";

export class IkeaCategoryListPageVisitor implements IkeaPageVisitor {
  supports(url: string): boolean {
   // return url.includes("/api/v1/stock/524/");
    return url.includes("/ru/ru/cat/");
  }

 visitPage({ content, enqueueUrl }): void {
    const $ = cheerio.load(content, { xmlMode: true });//
    
    /*for ids
    let cont=JSON.parse(content)['data'];
    let arr1=[];
    let count=0;
    for(let item of cont['productIds'])
    {
        arr1.push(item);
        let url_tek='https://www.ikea.com/ru/ru/catalog/products/'+item+'/';//?type=xml&dataset=normal,allImages,prices,attributes
        //console.log(url_tek);
       // url_tek='https://www.ikea.com/ru/ru/p/hemnes-karkas-krovati-belaya-morilka-luroy-s39210808/';
       function delay(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }
      if(count<50)
        enqueueUrl(url_tek);
      else if(count<200)   
        delay(count*200).then(() => enqueueUrl(url_tek));
      else if(count<1500)   
        delay(count*100).then(() => enqueueUrl(url_tek));
      else if(count<10000)
       delay(count*100).then(() => enqueueUrl(url_tek));
      else delay(count*100).then(() => enqueueUrl(url_tek));

       count++;
       //if(count>250) break;
    }
end for ids*/
    //console.log('Длина массива'+arr1.length);

    // категории
    // let arr=$(".plp-navigation-slot-wrapper .vn-accordion__item li > a").toArray()
    //   .map(element => $(element).attr("href"));
    //только главные. Из картинок. 
    // let arr=$(".plp-navigation-slot-wrapper .vn-accordion__image > a").toArray()
    //   .map(element => $(element).attr("href"));
      //только главные. Из картинок. 
      let arr = $(".plp-navigation-slot-wrapper .vn-accordion__image").toArray()
            .map(element => $(element).attr("href"))
      .map(enqueueUrl);

      let arr2 =$(".vn__nav__link").toArray()
      .map(element => $(element).attr("href"));
     // console.log(arr2);
      arr2.map(enqueueUrl);
      
      //enqueueUrl(arr[0]);
  }
}
