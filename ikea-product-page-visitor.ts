import { IkeaProductV2 } from "./ikea-product-v2";
import { IkeaPageVisitor } from "./ikea-page-visitor";
import * as cheerio from "cheerio";

export class IkeaProductPageVisitor implements IkeaPageVisitor {
  private visitedProducts = new Set<string>();
  private errorProducts = 0;
  getVisitedProductsCount(): number {
    return this.visitedProducts.size;
  }
  getErrorProductsCount(): number {
    return this.errorProducts;
  }

  supports(url: string): boolean {
    if(url.includes("/catalog/products/"))return url.includes("/catalog/products/");
    return url.includes("/ru/ru/p/");
  }

  visitPage({ url, content, enqueueUrl, handleProduct }): void {
    const $ = cheerio.load(content,{ decodeEntities: false });
    // варианты товара
    $("div.range-revamp-product-variation > a").toArray()
      .map(element => $(element).attr("href"))
      .map(enqueueUrl);
    // товар
    const product = this.extractProduct(url, $);
    //console.log("id2="+product.id);
    if(product.id=='0')this.errorProducts++;
    if (product.id!='0') {
      //enqueueUrl(`/ru/ru/iows/catalog/availability/${product.id}/`, { product });
      handleProduct(product);
      this.visitedProducts.add(product.id);
    }

    /*убираю для парсинга по ид из апи.*/
    let varurl1_1=[];
    if(product.variations!='')
      {
        let variations_1 = JSON.parse(product.variations);
        if (variations_1.variation instanceof Array)
        for (let i = 0; i < variations_1.variation.length; i++) {
          varurl1_1 = variations_1.variation[i].options.map(el => el.url);

         // console.dir(varurl1_1);
          varurl1_1.map(enqueueUrl);
        }
        
    }
  }

  extractProduct(url: string, $: CheerioStatic): IkeaProductV2 {
    const id = String($("div.product-pip.js-product-pip").data("product-id"));
   // console.log('id='+typeof id);
    if(id == 'undefined')  return {
        id:'0',
        name:'0',
        shortText:'0',
        shortText_2:'0',
        description:'0',
        number:'0',
        breadCrumbList:[],
        breadCrumbListHref:[],
        breadCrumbListID:[],
        imageList:[],
        price:0,
        weight:0,
        variations:'0',
        environmentAndMaterial:'0',
        measurement:'0',
        recommendationsSCU:'0',
        url,
      };

    //cat1
    const breadCrumbList = $("div.bc-breadcrumb li.bc-breadcrumb__list-item:not(.bc-breadcrumb__list-item--active) > a").toArray()
        .map(element => $(element).text().trim())
        .filter(text => text.length > 0)
        .slice(1,-1);
    const breadCrumbListHref = $("div.bc-breadcrumb li.bc-breadcrumb__list-item:not(.bc-breadcrumb__list-item--active) > a").toArray()
         .map(element => $(element).attr("href").trim())
         .filter(text => text.length > 0)
         .slice(1,-1);
    //поведение этого параметра надо исследовать. Чтобы список ид соотвествовал. Текст потом разделять и разбивать. Первую пропускать, если попадает. Продукт убирать. Или уже в скрипте обработки. Подумаю. 
    const breadCrumbListID = $("div.bc-breadcrumb li.bc-breadcrumb__list-item:not(.bc-breadcrumb__list-item--active) > a").toArray()
         .map(element => $(element).attr("data-tracking-label"))
         .filter(text => text!=undefined && text.length > 0)
         .slice(1);
    //type2 //text1
    const name = $("div.range-revamp-header-section__title--big").text()
        .replace(/&#34;/g, "")
        .replace(/\n/g, "");
    // imd2D
    const imageList = $("div.js-range-media-grid div.range-revamp-media-grid__media-container  img").toArray()
        .map(element => $(element).attr("src"))
        .map(src => src.split("?")[0]);

    // const imageList = $(".range-revamp-aspect-ratio-image  img").toArray()
    //     .map(element => $(element).attr("src"))
    //     .map(src => src.split("?")[0]);
      //  console.log(imageList);
    // shortText
    // careInstructions
    let shortText = $(".range-revamp-header-section__description .range-revamp-header-section__description-text").text().trim()
        .replace(/&#34;/g, "")
        .replace(/;/g, "")
        .replace(/\n\s*/g, "\\n");
    const shortText_1_measurement = $(".range-revamp-header-section__description .range-revamp-header-section__description-measurement").text().trim()
        .replace(/&#34;/g, "")
        .replace(/;/g, "")
        .replace(/\n\s*/g, "\\n");
    shortText = shortText + ", " + shortText_1_measurement;
    //дополнительное краткое описание.
    let shortText_2 = $(".range-revamp-product-summary .range-revamp-product-summary__description").text().trim()
        .replace(/&#34;/g, "")
        .replace(/;/g, "")
        .replace(/\n\s*/g, "\\n");
    // priceH
    let price1 = $("div.js-price-package  div.range-revamp-pip-price-package__main-price span.range-revamp-price__integer").text() 
        .match(/[\d\.]/g);
         if(price1==null) price1=['0'];
        let price=price1.join('');
    // text2 // type1
    let type = $("div.product-pip__top-container span.range__text-rtl").text()
        .trim()
        .replace(/&#34;/g, "");
    // text5
    const dimensions = $(".range-revamp-chunky-header__subtitle").text().trim()
        .replace(/:\s*\n/g, ":")
        .replace(/ \s{2,}/g, " ")
        .replace(/\n\s*/g, "\\n");
    // art, scu
    const partNumber = $(".range-revamp-product-summary .range-revamp-product-identifier__value").text();//range-revamp-product-identifier__number
    // const partNumberList = partNumber.match(/(\d){1,3}/g);
    // // Arthome
    // const itemNumber = partNumberList.join(".");
    const itemNumber = partNumber;
    //full description
    const description_1 = $(".range-revamp-product-details .range-revamp-product-details__container").first().html();
   // console.log("s"+description_1);
    let description = description_1;//.trim()
        // .replace(/;/g, "")
        // .replace(/\u2013/g, "-")
        // .replace(/ \s{2,}/g, " ")
        // .replace(/\n\s*/g, "\\n");
    // размеры.
    let measurement_1 = $(".range-revamp-product-dimensions   .range-revamp-product-dimensions__list").html();
    if(measurement_1==null) measurement_1='';
    let measurement = measurement_1.trim()
        .replace(/;/g, "")
        .replace(/\u2013/g, "-")
        .replace(/ \s{2,}/g, " ")
        .replace(/\n\s*/g, "\\n");
    // размеры. С картинками. Полностью. 
    //const measurement = $(".range-revamp-product-dimensions").html();
    //Материал и упаковка
    let environmentAndMaterial_1 = $("#SEC_product-details-material-and-care   ").html();
    
    let environmentAndMaterial ='';
    if(environmentAndMaterial_1 !== null)
    environmentAndMaterial="<h5 class='range-expandable__subheader'> Материалы </h5>"+environmentAndMaterial_1;
    const careInstructions_1 = $("#SEC_product-details-sustainability-and-environment").html();
    if(careInstructions_1 !== null)
       environmentAndMaterial = environmentAndMaterial + "<h5 class='range-expandable__subheader'> Окружающая среда </h5>"+ careInstructions_1;
   
    // const environmentAndMaterial=environmentAndMaterial_1.join(" ").trim()
    //     .replace(/&#34;/g, "")
    //     .replace(/;/g, "")
    //     .replace(/\s*\n\s*/g, "\\n");


    //////////////////////////////////////////
    //обработка вариаций.  js-product-variation-section
    const variation = $(".js-product-variation-section").toArray()
    .map(element => $(element).attr('data-initial-props').trim());
   // console.log(variation);
    let variatios_js='';
    let objVar={'variation':[]};
   // console.log(variation[0]);
    if(variation[0]!=undefined){
        let obj=JSON.parse(variation[0]);
        //console.dir(obj);
        //console.dir(obj, {depth: null, colors: true})
        
        for(let prop of obj['variations']){
            
            let tempObj={'options':[]};
            tempObj['title']=prop['title'];
            let tempObj2={};//{'title':'','url':'','id':''};
            for(let temp of prop['options']){
                tempObj2={};//{'title':'','url':'','id':''};
                //console.log(temp);
                tempObj2['title']=temp['title'];
                tempObj2['url']=temp['url'];
                tempObj2['id']=temp['linkId'];

                const partNumberList = tempObj2['id'].match(/(\d){1,3}/g);
                const itemNumber = partNumberList.join(".");
                tempObj2['scu']=itemNumber;
                tempObj['options'].push(tempObj2);
            }
        
            objVar['variation'].push(tempObj);
    }
    //console.dir(objVar, {depth: null, colors: true});
    // variatios_js=JSON.stringify(objVar);
}

//обработка вариаций. 2 - c картинками
const variation_2 = $(".js-product-style-picker").toArray()
.map(element => $(element).attr('data-initial-props').trim());
//console.log(variation_2);
let variatios_js_2='';
//console.log(variation_2[0]);
if(variation_2[0]!=undefined){
    let obj=JSON.parse(variation_2[0]);
    //console.dir(obj);
   // console.dir(obj, {depth: null, colors: true})
        
    let prop=obj;
        let tempObj={'options':[]};
        tempObj['title']=prop['title'];
        let tempObj2={};//{'title':'','url':'','id':''};
        for(let temp of prop['options']){
            tempObj2={};//{'title':'','url':'','id':''};
            //console.log(temp);
            tempObj2['title']=temp['title'];
            tempObj2['url']=temp['url'];
            tempObj2['id']=temp['linkId'];

            const partNumberList = tempObj2['id'].match(/(\d){1,3}/g);
            const itemNumber = partNumberList.join(".");
            tempObj2['scu']=itemNumber;
            tempObj['options'].push(tempObj2);
        }
    
        objVar['variation'].push(tempObj);


 
}
//console.dir(objVar, {depth: null, colors: true});
if(objVar['variation'].length>0)
  variatios_js=JSON.stringify(objVar);
 // console.log(variatios_js);
    ///////////////////////////////////////////////
    //упаковки. Расчет веса.
    const packaging = $("#SEC_product-details-packaging  .range-revamp-product-details__container > .range-revamp-product-details__container").toArray()
    .map(element => $(element).html().trim());
   // console.log(packaging);
    let count = 0;
    let arr_w = [[],[]];
    for (let pack of packaging) {
        //console.log(pack);
        const $_1 = cheerio.load(pack);
        const arr = $_1(".range-revamp-product-details__label").toArray().map(element => $(element).text().trim());
        //console.log(arr);
        for (let item of arr) {
            if (item.includes("Вес")) {
                const arr1 = item.split(":").map(el => el.trim());
               // console.log(arr1);
                const arr2 = arr1[1].split(" ").map(el => el.trim());
               // console.log(arr2);
                arr_w[0][count] = arr2[0];
            }
            if (item.includes("Упаковки")) {
                const arr1 = item.split(":").map(el => el.trim());
                arr_w[1][count] = arr1[1];
            }
        }
        count++;
    }
   // console.log(arr_w);
    let sum = 0;
        for (let i=0;i<arr_w[0].length;i++)
         {   
            sum += arr_w[0][i]* arr_w[1][i];
         }
    const weight = sum;
    //console.log(weight);
    //////////////////////////////////////////////
    //Do you like не найден пока. Делаю другой Хорошо сочетается с
    //data-ref-id  - ид. Возможно на него поменять.
    const recommendationsIDs = $(".range-revamp-recommendations-wrapper .range-revamp-product-compact").toArray()
        .map(element => $(element).attr("data-product-number"));//.text());//.trim())
       // .filter(text => text.length > 0);
       // console.log(recommendationsIDs);
    //под вопросом. Проверить.
    // let recommendationsSCUs = [];
    // for (let id of recommendationsIDs) {
    //     const partNumberList = id.match(/(\d){1,3}/g);
    //     // // Arthome
    //     recommendationsSCUs.push(partNumberList.join("."));
    // }
    // let recommendationsSCU = recommendationsSCUs.join("|");
    // console.log(recommendationsSCU);
    
    let recommendationsSCU='';
   /* console.log({
      id,
      name,
      shortText,
      shortText_2,
      description,
      number: itemNumber,
      breadCrumbList,
      breadCrumbListHref,
      breadCrumbListID,
      imageList,
      price: +price,
      weight,
      variations:variatios_js,
      environmentAndMaterial,
      measurement,
      recommendationsSCU,
      url,
    });*/
    return {
      id,
      name,
      shortText,
      shortText_2,
      description,
      number: itemNumber,
      breadCrumbList,
      breadCrumbListHref,
      breadCrumbListID,
      imageList,
      price: +price,
      weight,
      variations:variatios_js,
      environmentAndMaterial,
      measurement,
      recommendationsSCU,
      url,
    };
}
}
