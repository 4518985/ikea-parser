import { default as PQueue } from "p-queue";
import * as moment from "moment";
import { IkeaProductAvailability, IkeaProductAvailabilityForecast } from "./ikea-product-availability";
import { IkeaProductAvailabilityHandler } from "./ikea-product-availability-handler";
import { DB } from "../core/database/database-connection";
import { Logger } from "../core/logger/logger";

interface PriceItemRecord {
  priceId: number;
  productId: string;
  price: number;
  familyPrice?: number;
  availableStock: number;
  restockDate: string;
  forecast1day: number;
  forecast2day: number;
  forecast3day: number;
  forecast4day: number;
}

interface PriceFindItItemRecord {
  priceId: number;
  productId: string;
  partNumber: string;
  quantity: number;
  type: string;
  box: string;
  shelf: string;
}

export class IkeaDbPriceHandler implements IkeaProductAvailabilityHandler {
  private records: PriceItemRecord[] = [];
  private recordQueue = new PQueue({
    concurrency: 3
  });
  private findItRecords: PriceFindItItemRecord[] = [];
  private findItRecordQueue = new PQueue({
    concurrency: 3
  });
  private priceId: number;

  constructor(private db: DB, private logger: Logger) {}

  async initialize(): Promise<void> {
    this.priceId = await this.insertPrice(moment().format("YYYY-MM-DD HH:mm:ss"));
  }

  async handle(availability: IkeaProductAvailability): Promise<void> {
    const forecast: IkeaProductAvailabilityForecast = availability.forecast || {};
    this.recordQueue.add(() =>
      this.insertRecord({
        priceId: this.priceId,
        productId: availability.productId,
        price: availability.price,
        familyPrice: undefined,
        availableStock: availability.availableStock || 0,
        restockDate: availability.restockDate || null,
        forecast1day: forecast[1] || 0,
        forecast2day: forecast[2] || 0,
        forecast3day: forecast[3] || 0,
        forecast4day: forecast[4] || 0,
      }).catch(error => this.logger.error(error))
    );
    const findItList = availability.findItList || [];
    findItList.forEach(findIt => 
      this.findItRecordQueue.add(() => 
        this.insertFindItRecord({
          priceId: this.priceId,
          productId: availability.productId,
          partNumber: findIt.partNumber || null,
          quantity: findIt.quantity || null,
          type: findIt.type || null,
          box: findIt.box || null,
          shelf: findIt.shelf || null,
        }).catch(error => this.logger.error(error))
      )
    );
    return Promise.resolve();
  }

  async finalize(): Promise<void> {
    this.recordQueue.add(() =>
      this.insertRecords(this.records).catch(error => this.logger.error(error))
    );
    this.findItRecordQueue.add(() =>
      this.insertFindItRecords(this.findItRecords).catch(error => this.logger.error(error))
    );
    await this.recordQueue.onIdle();
    await this.findItRecordQueue.onIdle();
    return Promise.resolve();
  }

  private async insertPrice(createDateTime: string): Promise<number> {
    return this.db.query(
      `
        INSERT INTO prices
        SET ?`,
        { createDateTime }
      )
      .then(result => Number(result.insertId));
  }

  private async insertRecords(records: PriceItemRecord[]) {
    if (records.length === 0) {
      return Promise.resolve();
    }

    return this.db.query(
      `
        INSERT INTO price_items
            (priceId, productId, price, availableStock, restockDate, forecast1day, forecast2day, forecast3day, forecast4day)
        VALUES ?`,
      [
        records.map(record => {
          return [
            record.priceId,
            record.productId,
            record.price,
            record.availableStock,
            record.restockDate,
            record.forecast1day,
            record.forecast2day,
            record.forecast3day,
            record.forecast4day,
          ];
        })
      ]
    );
  }

  private async insertRecord(record: PriceItemRecord) {
    let result = Promise.resolve();
    this.records.push(record);
    if (this.records.length >= 100) {
      result = this.insertRecords(this.records);
      this.records.length = 0;
    }

    return result;
  }

  private async insertFindItRecords(findItRecords: PriceFindItItemRecord[]) {
    if (findItRecords.length === 0) {
      return Promise.resolve();
    }

    return this.db.query(
      `
        INSERT INTO price_find_it_items
            (priceId, productId, partNumber, quantity, type, box, shelf)
        VALUES ?`,
      [
        findItRecords.map(findItRecord => {
          return [
            findItRecord.priceId,
            findItRecord.productId,
            findItRecord.partNumber,
            findItRecord.quantity,
            findItRecord.type,
            findItRecord.box,
            findItRecord.shelf,
          ];
        })
      ]
    );
  }

  private async insertFindItRecord(findItRecord: PriceFindItItemRecord) {
    let result = Promise.resolve();
    this.findItRecords.push(findItRecord);
    if (this.findItRecords.length >= 100) {
      result = this.insertFindItRecords(this.findItRecords);
      this.findItRecords.length = 0;
    }

    return result;
  }
}
