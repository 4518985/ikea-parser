import { IkeaCategoriesHandler } from './ikea-categories-handler';
import { IkeaCategories } from './ikea-categories';
import { default as PQueue } from "p-queue";
import { IkeaCategoryListPageVisitor } from "./ikea-category-list-page-visitor";
import { IkeaPageVisitor } from "./ikea-page-visitor";
import { IkeaProductListPageVisitor } from "./ikea-product-list-page-visitor";
import { IkeaProductListPageVisitor_All } from "./ikea-all-products-visitor";
import { IkeaProductPageVisitor } from "./ikea-product-page-visitor";
import { IkeaProductAvailabilityHandler } from "./ikea-product-availability-handler";
import { IkeaProductAvailability } from "./ikea-product-availability";
import { IkeaProductAvailabilityVisitor } from "./ikea-product-availability-visitor";
import { IkeaProductV2 } from "./ikea-product-v2";
import { IkeaProductV2Handler } from "./ikea-product-v2-handler";
import { WebPageParser, WebPageParserOptions } from "../core/parser/web-page-parser";
import { HttpClientProvider } from "../core/http/http-client-provider";
import { Logger } from "../core/logger/logger";
import { DB } from "../core/database/database-connection";
import * as cheerio from "cheerio";

export class IkeaParser extends WebPageParser {
  private productPageVisitor = new IkeaProductPageVisitor();
  private requestQueue = new PQueue({
    concurrency: 10
  });
  private startTime: [number, number];

  private countError = 0;
  
  constructor(
    protected httpClientProvider: HttpClientProvider,
    protected logger: Logger,
    protected db: DB,
    protected options: WebPageParserOptions,
    protected catHandlers: IkeaCategoriesHandler[] = [],
    protected productHandlers: IkeaProductV2Handler[] = [],
    protected productAvailabilityHandlers: IkeaProductAvailabilityHandler[] = []
  ) {
    super(httpClientProvider, logger, options);
  }

  async handleStartPageUrl(startPageUrl: string): Promise<void> {
    const visitors: IkeaPageVisitor[] = [
      new IkeaCategoryListPageVisitor(),
      new IkeaProductListPageVisitor(),
      new IkeaProductListPageVisitor_All(),
      this.productPageVisitor,
      new IkeaProductAvailabilityVisitor(),
    ];
    const handleProduct = (product: IkeaProductV2): void => {
      this.productHandlers.forEach(handler =>
        this.requestQueue.add(() => handler.handle(product),{priority:2})
      );
    };
    const handleCategory = (cat: IkeaCategories): void => {
      this.catHandlers.forEach(handler =>
        this.requestQueue.add(() => handler.handle(cat))
      );
    };
    const handleProductAvailability = (productAvailability: IkeaProductAvailability): void => {
      this.productAvailabilityHandlers.forEach(handler =>
        this.requestQueue.add(() => handler.handle(productAvailability))
      );
    };
    const visitedUrls = new Set<string>();
    let tek_gran_show=1000;
    let offset_show=1000;
    const enqueueUrl = (url: string, context: { product?: IkeaProductV2; } = {}) => {
      if (!url || visitedUrls.has(url)) {
        return;
      }
      
      visitedUrls.add(url);
      //console.log(visitedUrls.size);
      this.requestQueue.add(() => {
        return this.executeRequest(url)
          .then((content: string) => {
            visitors.forEach(visitor => {
              try {
                if (visitor.supports(url)) {
                  //const db=this.db;
                  const logger=this.logger;
                  visitor.visitPage({ url, content, enqueueUrl, handleProduct, handleProductAvailability,handleCategory,db:this.db,logger, context });
                  //console.log(`Отпарсено: ${this.productPageVisitor.getVisitedProductsCount()}`);
                  //console.log(`Нет в базе: ${this.productPageVisitor.getErrorProductsCount()}`);
                  if(this.productPageVisitor.getVisitedProductsCount()>tek_gran_show){
                    tek_gran_show+=offset_show;
                    console.log("<br><b>Отпарсено:"+this.productPageVisitor.getVisitedProductsCount()+"</b>");
                    if(this.countError>0) { 
                      console.log(`Ошибок чтения ${this.countError}`);
                      this.countError=0;
                  }
                  }
                }
              } catch (error) {
                this.onError({ url, error });
              }
            });
          })
          .catch(error =>{ 
            this.onError({ url, error })
            this.countError= this.countError+1;
        
        })
      });
    };

    await this.truncateTable();
    await this.truncateTable_CAT();
    this.readIdsFromApi({enqueueUrl});
    await this.requestQueue.onIdle();
   //enqueueUrl(startPageUrl, {});
    //await this.requestQueue.onIdle();
   // await this.readIdsFromApiError({enqueueUrl});
    //await this.requestQueue.onIdle();
    return Promise.resolve();
  }

  //чтение ид из апи.
  private  readIdsFromApi({enqueueUrl}): Promise<any> { 
    const startPageUrl='https://ikeasources.ru/api/v1/stock/524/range/export';
   
    this.requestQueue.add(() => {
    return this.executeRequest(startPageUrl)
    .then((content: string) => {

      const $ = cheerio.load(content, { xmlMode: true });
    
        let cont=JSON.parse(content)['data'];
        let arr1=[];
        let count=0;
        function delay(ms) {
          return new Promise(resolve => setTimeout(resolve, ms));
        }
        for(let item of cont['productIds'])
        {
            //arr1.push(item);
            let url_tek='https://www.ikea.com/ru/ru/catalog/products/'+item+'/';//?type=xml&dataset=normal,allImages,prices,attributes
            //console.log(url_tek);
          // url_tek='https://www.ikea.com/ru/ru/p/hemnes-karkas-krovati-belaya-morilka-luroy-s39210808/';
          
          //if(count<50)
          /* if(item != '20351153' && item != '00351154' && item != '70351155' && item != '50351156' && item != '80461455' && item !='30351157')continue;*/
            enqueueUrl(url_tek);
          // else if(count<200)   
          //   delay(count*200).then(() => enqueueUrl(url_tek));
          // else if(count<1500)   
          //   delay(count*100).then(() => enqueueUrl(url_tek));
          // else if(count<10000)
          // delay(count*100).then(() => enqueueUrl(url_tek));
          // else delay(count*100).then(() => enqueueUrl(url_tek));

          count++;
          if(count>500) break;
        }
      
    })
    .catch(error =>{ 
      this.onError({ startPageUrl, error })
      //this.countError= this.countError+1;
  
  });
  });
    return Promise.resolve();
  }

  //допарсинг ошибочных. Перепарсинг.
  private  async readIdsFromApiError({enqueueUrl}): Promise<any> { 

    const items = await this.db.query(
      `SELECT id FROM products_v2` );

    const startPageUrl='https://ikeasources.ru/api/v1/stock/524/range/export';
   
    this.requestQueue.add(() => {
    return this.executeRequest(startPageUrl)
    .then((content: string) => {
      const $ = cheerio.load(content, { xmlMode: true });
    
        let cont=JSON.parse(content)['data'];
        let arr1=[];
        let count=0;
        function delay(ms) {
          return new Promise(resolve => setTimeout(resolve, ms));
        }
        let items_el=[];
        for(let el of items){
          items_el[el['id']]=el['id'];
        }
        
        for(let item of cont['productIds'])
        {
          console.log(item);
          if(items_el[item])continue;
            else arr1.push(item);
          //  continue;
            let url_tek='https://www.ikea.com/ru/ru/catalog/products/'+item+'/';//?type=xml&dataset=normal,allImages,prices,attributes
           // console.log(url_tek);
          // url_tek='https://www.ikea.com/ru/ru/p/hemnes-karkas-krovati-belaya-morilka-luroy-s39210808/';
          
          //if(count<50)
          // if(item != '20351153' && item != '00351154' && item != '70351155' && item != '50351156' && item != '80461455' && item !='30351157')continue;
            enqueueUrl(url_tek);
          // else if(count<200)   
          //   delay(count*200).then(() => enqueueUrl(url_tek));
          // else if(count<1500)   
          //   delay(count*100).then(() => enqueueUrl(url_tek));
          // else if(count<10000)
          // delay(count*100).then(() => enqueueUrl(url_tek));
          // else delay(count*100).then(() => enqueueUrl(url_tek));

          count++;
          console.log(count);
         // if(count>250) break;
        }
        //console.log('Длина: '+arr1.length);
      
    })
    .catch(error => this.onError({ startPageUrl, error }));
  });
    return Promise.resolve();
  }


  //for testing
  private async truncateTable(): Promise<any[]> {
    return this.db.query(
      `TRUNCATE table products_v2`
    );
  }
  private async truncateTable_CAT(): Promise<any[]> {
    return this.db.query(
      `TRUNCATE table categories`
    );
  }

  async onInitialize(): Promise<any> {
    this.logger.info(`<br><b>Парсер запущен</b>`);
    this.startTime = process.hrtime();
    await Promise.all(this.catHandlers.map(handler => handler.initialize()));
    await Promise.all(this.productHandlers.map(handler => handler.initialize()));
    await Promise.all(this.productAvailabilityHandlers.map(handler => handler.initialize()));
    return Promise.resolve();
  }

  async onFinalize(options): Promise<void> {
    await Promise.all(this.productAvailabilityHandlers.map(handler => handler.finalize()));
    await Promise.all(this.productHandlers.map(handler => handler.finalize()));
    await Promise.all(this.catHandlers.map(handler => handler.finalize()));
    const stopTime = process.hrtime(this.startTime);
    this.logger.info(`<br><b>Парсинг завершен. Отпарсено: ${this.productPageVisitor.getVisitedProductsCount()}</b>`);
    this.logger.info(`<br>Время работы: ${stopTime[0]} s`);
    return super.onFinalize(options);
  }

  async onError(error: any): Promise<void> {
    this.logger.error(error);
    return Promise.resolve();
  }
}
