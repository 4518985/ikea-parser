import { default as PQueue } from "p-queue";
import { IkeaCategories } from "./ikea-categories";
import { IkeaCategoriesHandler } from "./ikea-categories-handler";
import { DB } from "../core/database/database-connection";
import { Logger } from "../core/logger/logger";

interface CategoryRecord {
    id: string;
    name: string;
    url: string;
    parent: string;
    parent2: string;
    parent3: string;
    count_product: string;
    count_product2: string;
}

export class IkeaDBCategoriesHandler implements IkeaCategoriesHandler {
  private records: CategoryRecord[] = [];
  private recordQueue = new PQueue({
    concurrency: 3
  });

  constructor(private db: DB, private logger: Logger) {}

  async initialize(): Promise<void> {}

  handle(cat: IkeaCategories): Promise<void> {
    this.recordQueue.add(() =>
      this.insertRecord({
        id: cat.id,
        name: cat.name,
        url: cat.url,
        parent: cat.parent,
        parent2: cat.parent2,
        parent3: cat.parent3,
        count_product: cat.count_product,
        count_product2: cat.count_product2
      }).catch(error => this.logger.error(error))
    );
    return Promise.resolve();
  }

  async finalize(): Promise<void> {
    this.recordQueue.add(() =>
      this.insertRecords(this.records).catch(error => this.logger.error(error))
    );
    await this.recordQueue.onIdle();
    return Promise.resolve();
  }

  private async insertRecords(records: CategoryRecord[]) {
    if (records.length === 0) {
      return Promise.resolve();
    }

    return this.db.query(
      `
        INSERT INTO categories
            (id, name, url, parent, parent2,parent3, count_products, count_products2
              )
        VALUES ?
        ON DUPLICATE KEY UPDATE
        id = VALUES(id),
        name = VALUES(name),
        url = VALUES(url),
        parent = VALUES(parent),
        parent2 = VALUES(parent2),
        parent3 = VALUES(parent3),
        count_products = VALUES(count_products),
        count_products2 = VALUES(count_products2)        
        `,
      [
        records.map(record => {
          return [
            record.id,
            record.name,
            record.url,
            record.parent,
            record.parent2,
            record.parent3,
            record.count_product,
            record.count_product2
          ];
        })
      ]
    );
  }

  private async insertRecord(record: CategoryRecord) {
    let result = Promise.resolve();
    this.records.push(record);
    if (this.records.length >= 100) {
      result = this.insertRecords(this.records);
      this.records.length = 0;
    }

    return result;
  }
}
