export interface IkeaProductV2 {
  id: string;
  name: string;
  number: string;
  price: number;
  shortText: string;
  shortText_2: string;
  description: string;
  
  breadCrumbList: string[];
  breadCrumbListHref: string[];
  breadCrumbListID: string[];
  imageList: string[];
  
  weight: number;
  variations:string;
  environmentAndMaterial: string;
  measurement : string;
  recommendationsSCU:string;
  url: string;
}
