import { IkeaProductHandler } from "./ikea-product-handler";
import { IkeaProduct, IkeaProductPackageInfo } from "./ikea-product";
import { default as PQueue } from "p-queue";
import { DB } from "../core/database/database-connection";
import { Logger } from "../core/logger/logger";

interface ProductRecord {
  productId: string;
  productName: string;
  type: string;
  number: string;
  breadCrumbs: string;
  images: string;
  metric: string;
  benefit: string;
  careInst: string;
  materials: string;
  goodToKnow: string;
  validDesign: string;
  productUrl: string;
}

interface ProductPackageRecord {
  productId: string;
  articleNumber: string;
  idx: number;
  consumerPackNo: number;
  quantity: number;
  height: number;
  length: number;
  width: number;
  diameter: number;
  weight: number;
}

export class IkeaDbProductHandler implements IkeaProductHandler {
  private records: ProductRecord[] = [];
  private recordQueue = new PQueue({
    concurrency: 3
  });
  private packageRecords: ProductPackageRecord[] = [];
  private packageRecordQueue = new PQueue({
    concurrency: 3
  });

  constructor(private db: DB, private logger: Logger) {}

  async initialize(): Promise<void> {}

  handle(product: IkeaProduct): Promise<void> {
    this.recordQueue.add(() =>
      this.insertRecord({
        productId: product.productId,
        productName: product.productName,
        type: product.type,
        number: product.number,
        breadCrumbs: product.breadCrumbs,
        images: product.imageList.join(","),
        metric: product.metric,
        benefit: product.benefit,
        careInst: product.careInst,
        materials: product.materials,
        goodToKnow: product.goodToKnow,
        validDesign: product.validDesign,
        productUrl: product.productUrl
      }).catch(error => this.logger.error(error))
    );
    product.pakageInfoList.forEach((packageInfo: IkeaProductPackageInfo) => 
      this.packageRecordQueue.add(() =>
        this.insertPackageRecord({
          productId: product.productId,
          articleNumber: packageInfo.articleNumber,
          idx: packageInfo.idx,
          consumerPackNo: packageInfo.consumerPackNo,
          quantity: packageInfo.quantity,
          height: packageInfo.height,
          length: packageInfo.length,
          width: packageInfo.width,
          diameter: packageInfo.diameter,
          weight: packageInfo.weight,
        }).catch(error => this.logger.error(error))
      )
    );
    return Promise.resolve();
  }

  async finalize(): Promise<void> {
    this.recordQueue.add(() =>
      this.insertRecords(this.records).catch(error => this.logger.error(error))
    );
    this.packageRecordQueue.add(() =>
      this.insertPackageRecords(this.packageRecords).catch(error => this.logger.error(error))
    );
    await this.recordQueue.onIdle();
    await this.packageRecordQueue.onIdle();
    return Promise.resolve();
  }

  private async insertRecords(records: ProductRecord[]) {
    if (records.length === 0) {
      return Promise.resolve();
    }

    return this.db.query(
      `
        INSERT INTO products
            (id, name, type, number, breadCrumbs, images, metric, benefit, careInst, materials, goodToKnow, validDesign, url)
        VALUES ?
        ON DUPLICATE KEY UPDATE
            name = VALUES(name),
            type = VALUES(type),
            number = VALUES(number),
            breadCrumbs = VALUES(breadCrumbs),
            images = VALUES(images),
            metric = VALUES(metric),
            benefit = VALUES(benefit),
            careInst = VALUES(careInst),
            materials = VALUES(materials),
            goodToKnow = VALUES(goodToKnow),
            validDesign = VALUES(validDesign),
            url = VALUES(url)
        `,
      [
        records.map(record => {
          return [
            record.productId,
            record.productName,
            record.type,
            record.number,
            record.breadCrumbs,
            record.images,
            record.metric,
            record.benefit,
            record.careInst,
            record.materials,
            record.goodToKnow,
            record.validDesign,
            record.productUrl
          ];
        })
      ]
    );
  }

  private async insertRecord(record: ProductRecord) {
    let result = Promise.resolve();
    this.records.push(record);
    if (this.records.length >= 100) {
      result = this.insertRecords(this.records);
      this.records.length = 0;
    }

    return result;
  }

  private async insertPackageRecords(packageRecords: ProductPackageRecord[]) {
    if (packageRecords.length === 0) {
      return Promise.resolve();
    }

    return this.db.query(
      `
        INSERT INTO products_package
            (productId, articleNumber, idx, consumerPackNo, quantity, height, length, width, diameter, weight)
        VALUES ?
        ON DUPLICATE KEY UPDATE
            consumerPackNo = VALUES(consumerPackNo),
            quantity = VALUES(quantity),
            height = VALUES(height),
            length = VALUES(length),
            width = VALUES(width),
            diameter = VALUES(diameter),
            weight = VALUES(weight)
        `,
      [
        packageRecords.map(packageRecord => {
          return [
            packageRecord.productId,
            packageRecord.articleNumber,
            packageRecord.idx,
            packageRecord.consumerPackNo,
            packageRecord.quantity,
            packageRecord.height,
            packageRecord.length,
            packageRecord.width,
            packageRecord.diameter,
            packageRecord.weight,
          ];
        })
      ]
    );
  }

  private async insertPackageRecord(packageRecord: ProductPackageRecord) {
    let result = Promise.resolve();
    this.packageRecords.push(packageRecord);
    if (this.packageRecords.length >= 100) {
      result = this.insertPackageRecords(this.packageRecords);
      this.packageRecords.length = 0;
    }

    return result;
  }
}
