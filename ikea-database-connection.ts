import { config } from "../config";
import { DB } from "../core/database/database-connection";

export class IkeaDB extends DB {
    constructor() {
        super(config.get("ikeaDatabase"));
    }
}
